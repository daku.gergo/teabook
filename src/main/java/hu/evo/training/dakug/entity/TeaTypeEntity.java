package hu.evo.training.dakug.entity;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "teatype")
@Entity
public class TeaTypeEntity implements Serializable {
    private static final long serialVersionUID = -1738827460029443627L;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TeaTypeEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
