package hu.evo.training.dakug.entity;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "user")
@NamedQueries({
        @NamedQuery(name=UserEntity.QUERY_FIND_BY_LOGIN,
                query="SELECT u FROM UserEntity u WHERE u.login=:"+UserEntity.PARAM_LOGIN)
})
@Entity
public class UserEntity implements Serializable {
    private static final long serialVersionUID = 7584677702084079470L;

    public static final String QUERY_FIND_BY_LOGIN = "UserEntity.findByLogin";
    public static final String PARAM_LOGIN         = "login";

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "salt")
    private String salt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Override
    public String toString() {
        return "TeaNoteEntity{" +
                "id=" + id +
                ", login='" + login + '\'' +
                '}';
    }
}
