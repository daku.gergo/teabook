package hu.evo.training.dakug.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "teanote")
@NamedQueries({
        @NamedQuery(name= TeaNoteEntity.QUERY_FIND_BY_CREATE_USER_ID,
                query="SELECT t FROM TeaNoteEntity t WHERE t.userEntity.id=:"+TeaNoteEntity.PARAM_CREATE_USER_ID)
})
@Entity
public class TeaNoteEntity implements Serializable {
    private static final long serialVersionUID = -4956294172798519141L;

    public static final String QUERY_FIND_BY_CREATE_USER_ID = "TeaNoteEntity.findByCreateUserId";
    public static final String PARAM_CREATE_USER_ID         = "createUserId";

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "create_user_id")
    @ManyToOne
    private UserEntity userEntity;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "tea_name")
    private String teaName;

    @Column(name = "teatype_id")
    private Integer teaTypeId;

    @Column(name = "score")
    private Integer score;

    @Column(name = "note")
    private String note;

    @Column(name = "water_temp")
    private Integer waterTemp;

    @Column(name = "water_mineral")
    private Integer waterMineral;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modify_date")
    private Date modifyDate;

    public TeaNoteEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getTeaName() {
        return teaName;
    }

    public void setTeaName(String teaName) {
        this.teaName = teaName;
    }

    public Integer getTeaTypeId() {
        return teaTypeId;
    }

    public void setTeaTypeId(Integer teaTypeId) {
        this.teaTypeId = teaTypeId;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getWaterTemp() {
        return waterTemp;
    }

    public void setWaterTemp(Integer waterTemp) {
        this.waterTemp = waterTemp;
    }

    public Integer getWaterMineral() {
        return waterMineral;
    }

    public void setWaterMineral(Integer waterMineral) {
        this.waterMineral = waterMineral;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    public String toString() {
        return "TeaNoteEntity{" +
                "id=" + id +
                ", userEntity=" + userEntity +
                ", createDate=" + createDate +
                ", teaName='" + teaName + '\'' +
                ", teaTypeId=" + teaTypeId +
                ", score=" + score +
                ", note='" + note + '\'' +
                ", waterTemp=" + waterTemp +
                ", waterMineral=" + waterMineral +
                ", modifyDate=" + modifyDate +
                '}';
    }
}
