package hu.evo.training.dakug.enums;

public enum SortingType {

    STANDARD("Mindegy"),
    DESC_BY_VALUE("Ertek szerint csokkeno"),
    DESC_BY_DATE("Datum szerint novekvo");

    public final String text;

    SortingType(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }


    @Override
    public String toString() {
        switch(this){
            case STANDARD:
                return "Standard";
            case DESC_BY_VALUE:
                return "Desc_by_value";
            case DESC_BY_DATE:
                return "Desc_by_date";
        }
        return null;
    }

    public static SortingType fromString(String text){
        for(SortingType st : SortingType.values()){
            if(st.text.equalsIgnoreCase(text)){
                return st;
            }
        }
        return null;
    }
}

