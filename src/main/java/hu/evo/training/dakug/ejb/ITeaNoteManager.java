package hu.evo.training.dakug.ejb;

import hu.evo.training.dakug.entity.TeaNoteEntity;
import hu.evo.training.dakug.enums.SortingType;
import hu.evo.training.dakug.exception.TeaNotesExeption;

import javax.ejb.Local;
import java.util.List;

@Local
public interface ITeaNoteManager {

    void createTeaNote(Integer userId, Integer teaId, Integer score, Integer temperature, Integer mineral, String note)  throws TeaNotesExeption;
    List<TeaNoteEntity> getUserTeaNotes(String username) throws TeaNotesExeption;
    void editTeaNote(Integer teaNoteId, Integer teaId, Integer score, Integer temperature, Integer mineral, String note)  throws TeaNotesExeption;
    List<TeaNoteEntity> getTopTeaNotes();
    List<TeaNoteEntity> getTopUsersNotes()  throws TeaNotesExeption;
    List<TeaNoteEntity> getFilteredTeaNotes(Integer teaId, SortingType sortingType) throws TeaNotesExeption;

}
