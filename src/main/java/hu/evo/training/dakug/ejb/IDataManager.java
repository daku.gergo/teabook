package hu.evo.training.dakug.ejb;

import hu.evo.training.dakug.entity.UserEntity;
import hu.evo.training.dakug.exception.TeaNotesExeption;

import javax.ejb.Local;
import java.util.List;

@Local
public interface IDataManager {

    void authenticate(String user, String password) throws TeaNotesExeption;
    <T> List<T> findAll(Class<T> klass);
    <T> T findById(Class<T> clazz, Integer id);
    UserEntity getUser(String userName);

}
