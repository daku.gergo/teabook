package hu.evo.training.dakug.ejb;

import hu.evo.training.dakug.entity.TeaNoteEntity;
import hu.evo.training.dakug.entity.TeaTypeEntity;
import hu.evo.training.dakug.entity.UserEntity;
import hu.evo.training.dakug.enums.SortingType;
import hu.evo.training.dakug.exception.TeaNotesExeption;
import hu.evo.training.dakug.exception.TechnicalException;
import hu.evo.training.dakug.log.LogUtil;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
public class TeaNoteManager implements ITeaNoteManager {

    private static Logger log = Logger.getLogger(TeaNoteManager.class);

    @PersistenceContext(unitName = "teanotes")
    private EntityManager entityManager;

    @EJB
    IDataManager dataManager;

    @Override
    public void createTeaNote(Integer userId, Integer teaId, Integer score, Integer temperature, Integer mineral, String note) throws TeaNotesExeption {//nem gond itt a lehetséges nullok átadasá?
        log.info(LogUtil.METHOD_ENTER.concat("createTeaNote()"));
        Date now = new Date();
        try{
            TeaNoteEntity teaNoteEntity = new TeaNoteEntity();
            teaNoteEntity.setUserEntity(dataManager.findById(UserEntity.class, userId));
            teaNoteEntity.setCreateDate(now);
            teaNoteEntity.setTeaName(dataManager.findById(TeaTypeEntity.class, teaId).getName());
            teaNoteEntity.setTeaTypeId(teaId);
            teaNoteEntity.setScore(score);
            teaNoteEntity.setNote(note);
            teaNoteEntity.setWaterTemp(temperature);
            teaNoteEntity.setWaterMineral(mineral);
            teaNoteEntity.setModifyDate(now);
            entityManager.persist(teaNoteEntity);
        }catch (Exception e) {
            log.error(e);
            throw new TechnicalException();
        }
        log.info(LogUtil.METHOD_SUCCESS.concat("createTeaNote()"));
    }

    @Override
    public List<TeaNoteEntity> getUserTeaNotes(String username) throws TeaNotesExeption {
        log.info(LogUtil.METHOD_ENTER.concat("getUserTeaNotes()"));
        UserEntity user = dataManager.getUser(username);
        try {
            TypedQuery<TeaNoteEntity> query = entityManager.createNamedQuery(TeaNoteEntity.QUERY_FIND_BY_CREATE_USER_ID, TeaNoteEntity.class);
            query.setParameter(TeaNoteEntity.PARAM_CREATE_USER_ID, user.getId());
            log.info(LogUtil.METHOD_SUCCESS.concat("getUserTeaNotes()"));
            return query.getResultList();

        }catch(Exception e){
            log.error(e);
            throw new TechnicalException();
        }
    }

    @Override
    public void editTeaNote(Integer teaNoteid, Integer teaId, Integer score, Integer temperature, Integer mineral, String note) throws TeaNotesExeption {
        log.info(LogUtil.METHOD_ENTER.concat("editTeaNote()"));
        try{
            TeaNoteEntity teaNoteEntity = dataManager.findById(TeaNoteEntity.class, teaNoteid);
            teaNoteEntity.setTeaName(dataManager.findById(TeaTypeEntity.class, teaId).getName());
            teaNoteEntity.setTeaTypeId(teaId);
            teaNoteEntity.setScore(score);
            teaNoteEntity.setNote(note);
            teaNoteEntity.setWaterTemp(temperature);
            teaNoteEntity.setWaterMineral(mineral);
            teaNoteEntity.setModifyDate(new Date());
            entityManager.merge(teaNoteEntity);
        }catch (Exception e) {
            log.error(e);
            throw new TechnicalException();
        }

        log.info(LogUtil.METHOD_SUCCESS.concat("editTeaNote()"));
    }

    @Override
    public List<TeaNoteEntity> getTopTeaNotes() {
        log.info(LogUtil.METHOD_ENTER.concat("getTopTeaNotes()"));
        String queryString = "SELECT t FROM TeaNoteEntity t order by t.score DESC";
        TypedQuery<TeaNoteEntity> query = (TypedQuery<TeaNoteEntity>) entityManager.createQuery(queryString);
        query.setMaxResults(3);
        log.info(LogUtil.METHOD_SUCCESS.concat("getTopTeaNotes"));
        return query.getResultList();
    }

    @Override
    public List<TeaNoteEntity> getTopUsersNotes() throws TeaNotesExeption{
        log.info(LogUtil.METHOD_ENTER.concat("getTopUserNotes()"));
        List<TeaNoteEntity> teaNoteList = new ArrayList<>();
        String queryString = "SELECT t FROM TeaNoteEntity t group by t.userEntity.id order by count(t.userEntity.id) DESC";
        try {
            TypedQuery<TeaNoteEntity> query = (TypedQuery<TeaNoteEntity>) entityManager.createQuery(queryString);
            query.setMaxResults(3);
            for (int i = 0; i < 3; i++) {
                teaNoteList.addAll(getUserTeaNotes(query.getResultList().get(i).getUserEntity().getLogin()));
            }
        }
        catch(Exception e){
            log.error(e);
            throw new TechnicalException();
        }
        log.info(LogUtil.METHOD_SUCCESS.concat("getTopUserNotes"));
        return teaNoteList;
    }

    @Override
    public List<TeaNoteEntity> getFilteredTeaNotes(Integer teaId, SortingType sortingType) throws TeaNotesExeption {
        log.info(LogUtil.METHOD_ENTER.concat("getFilteredTeaNotes()"));
        System.out.println(sortingType);
        List<TeaNoteEntity> teaNoteList;
        String queryString;
        if(teaId != 0){
            queryString = "SELECT t FROM TeaNoteEntity t where t.teaTypeId = " + teaId;
        }else{
            queryString = "SELECT t FROM TeaNoteEntity t";
        }

  /*      switch(sortingType) {
            case DESC_BY_DATE:
                queryString += " order by t.createDate DESC";
            break;
            case DESC_BY_VALUE:
                queryString += " order by t.score DESC";
            break;
        }*/
        if(sortingType.equals(SortingType.DESC_BY_DATE)){
            queryString += " order by t.createDate DESC";
        }else{
            queryString += " order by t.score DESC";
        }

        try {
            TypedQuery<TeaNoteEntity> query = (TypedQuery<TeaNoteEntity>) entityManager.createQuery(queryString);
            teaNoteList = query.getResultList();
        }
        catch(Exception e){
            log.error(e);
            throw new TechnicalException();
        }
        log.info(LogUtil.METHOD_SUCCESS.concat("getFilteredTeaNotes"));
        return teaNoteList;
    }
}
