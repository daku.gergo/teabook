package hu.evo.training.dakug.ejb;

import hu.evo.training.dakug.entity.UserEntity;
import hu.evo.training.dakug.log.LogUtil;
import hu.evo.training.pmamico.crypto.PBKDF2;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class PasswordManager implements IPasswordManager {

    private static Logger log = Logger.getLogger(DataManager.class);

    @PersistenceContext(unitName = "teanotes")
    private EntityManager entityManager;

    @EJB
    IDataManager dataManager;

    @Override
    public void generateSaltForAllUser() {
        log.info(LogUtil.METHOD_ENTER.concat("generateSaltForAllUser()"));
        List<UserEntity> users = dataManager.findAll(UserEntity.class);
        try {
            users.forEach(user -> {
                if(StringUtils.isEmpty(user.getSalt())){
                    user.setSalt(RandomStringUtils.random(10, true, true));
                    entityManager.merge(user);
                }
            });
        }catch(Exception e){
            log.error(e);
        }
        log.info(LogUtil.METHOD_SUCCESS.concat("generateSaltForAllUser()"));
    }

    @Override
    public void encryptAllPassword() {
        log.info(LogUtil.METHOD_ENTER.concat("encryptAllPassword()"));
        List<UserEntity> users = dataManager.findAll(UserEntity.class);

        users.forEach(user ->{
            if(user.getPassword().length() < 21){
                try{
                    String newPassword = PBKDF2.createHash(user.getPassword(), user.getSalt());
                    user.setPassword(newPassword);
                    entityManager.merge(user);
                }catch(Exception e){
                    log.error(e);
                }
            }
        });
        log.info(LogUtil.METHOD_SUCCESS.concat("encryptAllPassword()"));
    }
}
