package hu.evo.training.dakug.ejb;


import hu.evo.training.dakug.entity.UserEntity;
import hu.evo.training.dakug.exception.AuthException;
import hu.evo.training.dakug.exception.TeaNotesExeption;
import hu.evo.training.dakug.exception.TechnicalException;
import hu.evo.training.dakug.log.LogUtil;
import hu.evo.training.pmamico.crypto.PBKDF2;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class DataManager implements IDataManager {

    private static Logger log = Logger.getLogger(DataManager.class);

    @PersistenceContext(unitName = "teanotes")
    private EntityManager entityManager;

    @Override
    public void authenticate(String user, String password) throws TeaNotesExeption{
        log.info(LogUtil.METHOD_ENTER.concat("authenticate()"));
        UserEntity userEntity = null;
        try {
            TypedQuery<UserEntity> query = entityManager.createNamedQuery(UserEntity.QUERY_FIND_BY_LOGIN, UserEntity.class);
            query.setParameter(UserEntity.PARAM_LOGIN, user);
            List<UserEntity> resultList = query.getResultList();
            if(resultList.size()!=1 ||
                    !resultList.get(0).getPassword().equals(PBKDF2.createHash(password, resultList.get(0).getSalt()))){
                throw new AuthException();
            }

        } catch(TeaNotesExeption tne){
            throw tne;
        } catch (Exception e){
            log.error(e);
            throw new TechnicalException();
        }
        log.info(LogUtil.METHOD_SUCCESS.concat("authenticate()"));
    }

    public <T> List<T> findAll(Class<T> klass) {
        log.info(LogUtil.METHOD_ENTER.concat("findAll()"));
        Table tableAnnotation = klass.getAnnotation(Table.class);
        if (tableAnnotation != null) {
            String queryString = "SELECT a FROM ".concat(klass.getSimpleName()).concat(" a");
            TypedQuery<T> query = (TypedQuery<T>) entityManager.createQuery(queryString);
            log.info(LogUtil.METHOD_SUCCESS.concat("findAll()"));
            return query.getResultList();
        }
        log.info(LogUtil.METHOD_SUCCESS.concat("findAll()...table does not exist!"));
        return null;
    }

    public <T> T findById(Class<T> clazz, Integer id){
        log.info(LogUtil.METHOD_ENTER.concat("findById()"));
        Table tableAnnotation = clazz.getAnnotation(Table.class);
        if(tableAnnotation != null){
            String queryString = "SELECT A FROM ".concat((clazz.getSimpleName()).concat(" a").concat(" WHERE a.id = ").concat(id.toString()));
            TypedQuery<T> query = (TypedQuery<T>) entityManager.createQuery(queryString);
            return query.getSingleResult();
        }
        log.info(LogUtil.METHOD_SUCCESS.concat("findById()...table does not exist!"));
        return null;
    }

    @Override
    public UserEntity getUser(String userName) {
        log.info(LogUtil.METHOD_ENTER.concat("getUser()"));
        UserEntity ret = null;
        try{
            TypedQuery<UserEntity> query = entityManager.createNamedQuery(UserEntity.QUERY_FIND_BY_LOGIN, UserEntity.class);
            query.setParameter(UserEntity.PARAM_LOGIN, userName);
            List<UserEntity> resultList = query.getResultList();
            if(!resultList.isEmpty()){
                ret = resultList.get(0);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        log.info(LogUtil.METHOD_SUCCESS.concat("getUser()"));
        return ret;
    }
}
