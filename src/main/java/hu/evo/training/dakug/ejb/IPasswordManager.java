package hu.evo.training.dakug.ejb;

import javax.ejb.Local;

@Local
public interface IPasswordManager {

    void generateSaltForAllUser();
    void encryptAllPassword();
}
