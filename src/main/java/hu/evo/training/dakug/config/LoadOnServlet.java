package hu.evo.training.dakug.config;

import hu.evo.training.dakug.ejb.IPasswordManager;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class LoadOnServlet extends HttpServlet {

    @EJB
    IPasswordManager passwordManager;

    @Override
    public void init() throws ServletException {
        passwordManager.generateSaltForAllUser();
        passwordManager.encryptAllPassword();
    }
}
