package hu.evo.training.dakug.web;

import hu.evo.training.dakug.ejb.IDataManager;
import hu.evo.training.dakug.exception.TeaNotesExeption;
import hu.evo.training.dakug.log.LogUtil;
import hu.evo.training.dakug.security.LoginUtil;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet {
    private static Logger log = Logger.getLogger(LoginServlet.class);

    @EJB
    private IDataManager dataManager;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info(LogUtil.METHOD_ENTER.concat("GET"));
        log.info(LogUtil.METHOD_SUCCESS.concat("GET"));
        req.getRequestDispatcher("/jsp/login/init.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info(LogUtil.METHOD_ENTER.concat("POST"));
        try {
            String user     = req.getParameter("user");
            String password = req.getParameter("password");

            dataManager.authenticate(user, password);

            req.getSession().setAttribute(LoginUtil.ATTR_LOGGED_IN, user);

        } catch (TeaNotesExeption tne){
            req.setAttribute("result_message", tne.getMessage());
            req.getRequestDispatcher("/jsp/login/init.jsp").forward(req, resp);
            return;
        }

        log.info(LogUtil.METHOD_SUCCESS.concat("POST"));
        req.getRequestDispatcher("/jsp/loggedInMenu.jsp").forward(req, resp);
    }
}

