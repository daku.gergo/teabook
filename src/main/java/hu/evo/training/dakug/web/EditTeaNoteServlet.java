package hu.evo.training.dakug.web;

import hu.evo.training.dakug.ejb.IDataManager;
import hu.evo.training.dakug.ejb.ITeaNoteManager;
import hu.evo.training.dakug.entity.TeaNoteEntity;
import hu.evo.training.dakug.entity.TeaTypeEntity;
import hu.evo.training.dakug.exception.ScoreException;
import hu.evo.training.dakug.log.LogUtil;
import hu.evo.training.dakug.security.LoginUtil;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class EditTeaNoteServlet extends HttpServlet {

    private static Logger log = Logger.getLogger(EditTeaNoteServlet.class);

    @EJB
    IDataManager dataManager;

    @EJB
    ITeaNoteManager teaNoteManager;


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info(LogUtil.METHOD_ENTER.concat("GET"));
        Integer teaNoteId = Integer.parseInt(req.getParameter("teanoteid"));
        req.setAttribute("teanoteid", teaNoteId);
        TeaNoteEntity teaNoteEntity = dataManager.findById(TeaNoteEntity.class, teaNoteId);
        req.setAttribute("teanote", teaNoteEntity);
        List<TeaTypeEntity> teaList = dataManager.findAll(TeaTypeEntity.class);
        req.setAttribute("teas", teaList);
        log.info(LogUtil.METHOD_SUCCESS.concat("GET"));
        req.getRequestDispatcher("/jsp/userTeaNote/edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info(LogUtil.METHOD_ENTER.concat("POST"));
        Integer temperature = null;
        Integer mineral = null;
        try {
            Integer teaNoteId = Integer.parseInt(req.getParameter("teanoteid"));
            Integer teaId = Integer.parseInt(req.getParameter("teaId"));
            Integer score = Integer.parseInt(req.getParameter("score"));
            if(!req.getParameter("temperature").equals("")){
                temperature = Integer.parseInt(req.getParameter("temperature"));
            }
            if(!req.getParameter("mineral").equals("")){
                mineral = Integer.parseInt(req.getParameter("mineral"));
            }
            String note = req.getParameter("note");

            if(score > 100 || score < 1){
                throw new ScoreException();
            }

            teaNoteManager.editTeaNote(teaNoteId,  teaId, score, temperature, mineral, note);
            req.setAttribute("result_message", "A jegyzetet sikeresen módosította!");
        }catch(ScoreException se){
            req.setAttribute("result_message", se.getMessage());
            log.error(se);
        }catch (Exception e){
            e.printStackTrace();
        }
        log.info(LogUtil.METHOD_SUCCESS.concat("POST"));
        req.getRequestDispatcher("/jsp/userTeaNote/result.jsp").forward(req, resp);
    }
}
