package hu.evo.training.dakug.web;

import hu.evo.training.dakug.ejb.ITeaNoteManager;
import hu.evo.training.dakug.entity.TeaNoteEntity;
import hu.evo.training.dakug.enums.SortingType;
import hu.evo.training.dakug.log.LogUtil;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TopUsersServlet extends HttpServlet {

     private static Logger log = Logger.getLogger(TopUsersServlet.class);

    @EJB
    ITeaNoteManager teaNoteManager;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info(LogUtil.METHOD_ENTER.concat("GET"));
        List<TeaNoteEntity> topTeaNoteList = new ArrayList<>();
        try{
             topTeaNoteList = teaNoteManager.getTopUsersNotes();
        }catch(Exception e){
            log.error(e);
        }
        req.setAttribute("teaNotes", topTeaNoteList);
        log.info(LogUtil.METHOD_SUCCESS.concat("GET"));
        SortingType.valueOf("STANDARD");
        req.getRequestDispatcher("/jsp/common/topUsers.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
