package hu.evo.training.dakug.web;

import hu.evo.training.dakug.ejb.ITeaNoteManager;
import hu.evo.training.dakug.entity.TeaNoteEntity;
import hu.evo.training.dakug.entity.TeaTypeEntity;
import hu.evo.training.dakug.log.LogUtil;
import hu.evo.training.dakug.security.LoginUtil;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class UserTeaNoteServlet extends HttpServlet {

    private static Logger log = Logger.getLogger(UserTeaNoteServlet.class);

    @EJB
    ITeaNoteManager teaNoteManager;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info(LogUtil.METHOD_ENTER.concat("GET"));
        try {
            String userName = (String) req.getSession().getAttribute(LoginUtil.ATTR_LOGGED_IN);
            List<TeaNoteEntity> teaNotesList = teaNoteManager.getUserTeaNotes(userName);
            req.setAttribute("teanotes", teaNotesList);
        }catch(Exception e){
            req.setAttribute("result_message", e.getMessage());
            log.error(e);
        }
        log.info(LogUtil.METHOD_SUCCESS.concat("GET"));
        req.getRequestDispatcher("/jsp/userTeaNote/init.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
