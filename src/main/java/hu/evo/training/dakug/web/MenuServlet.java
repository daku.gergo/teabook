package hu.evo.training.dakug.web;

import hu.evo.training.dakug.log.LogUtil;
import hu.evo.training.dakug.security.LoginUtil;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MenuServlet extends HttpServlet {
    private static Logger log = Logger.getLogger(MenuServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info(LogUtil.METHOD_ENTER.concat("GET"));
        log.info(LogUtil.METHOD_SUCCESS.concat("GET"));
        if(req.getSession().getAttribute(LoginUtil.ATTR_LOGGED_IN) != null){
            req.getRequestDispatcher("/jsp/loggedInMenu.jsp").forward(req, resp);
        }else{
            req.getRequestDispatcher("/jsp/menu.jsp").forward(req, resp);
        }
    }
}
