package hu.evo.training.dakug.web;

import hu.evo.training.dakug.ejb.IDataManager;
import hu.evo.training.dakug.ejb.ITeaNoteManager;
import hu.evo.training.dakug.entity.TeaNoteEntity;
import hu.evo.training.dakug.entity.TeaTypeEntity;
import hu.evo.training.dakug.enums.SortingType;
import hu.evo.training.dakug.log.LogUtil;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BrowseServlet extends HttpServlet {

    private static Logger log = Logger.getLogger(BrowseServlet.class);

    @EJB
    IDataManager dataManager;

    @EJB
    ITeaNoteManager teaNoteManager;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info(LogUtil.METHOD_ENTER.concat("GET"));
        List<TeaNoteEntity> teaNoteList = dataManager.findAll(TeaNoteEntity.class);
        req.setAttribute("teaNotes", teaNoteList);
        fillUpSelects(req);
        log.info(LogUtil.METHOD_SUCCESS.concat("GET"));
        req.getRequestDispatcher("/jsp/common/browse.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info(LogUtil.METHOD_ENTER.concat("POST"));
        try{
            String sortingTypeString =  req.getParameter("sortType");
            log.debug("The sortingtypsString value is: " +sortingTypeString);
            SortingType sortingType = SortingType.fromString(sortingTypeString);
            log.debug("The sortingtypsvalue is: " +sortingType);

            Integer teaId = Integer.parseInt(req.getParameter("teaId"));
            log.debug("The Integer value is:"+ teaId);
            List<TeaNoteEntity> teaNoteList = teaNoteManager.getFilteredTeaNotes(teaId, sortingType);
            req.setAttribute("teaNotes", teaNoteList);
            fillUpSelects(req);
        }
        catch(Exception e){
            log.error(e);
        }
        log.info(LogUtil.METHOD_SUCCESS.concat("POST"));
        req.getRequestDispatcher("/jsp/common/browse.jsp").forward(req, resp);
    }

    private void fillUpSelects(HttpServletRequest req){
        List<TeaTypeEntity> teaList = dataManager.findAll(TeaTypeEntity.class);
        req.setAttribute("teas", teaList);
        List<SortingType> sortTypes = Arrays.asList(SortingType.values());
        req.setAttribute("sortingTypes", sortTypes);
    }
}
