package hu.evo.training.dakug.web;

import hu.evo.training.dakug.ejb.ITeaNoteManager;
import hu.evo.training.dakug.entity.TeaNoteEntity;
import hu.evo.training.dakug.log.LogUtil;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class TopNotesServlet extends HttpServlet {

    private static Logger log = Logger.getLogger(TopNotesServlet.class);

    @EJB
    ITeaNoteManager teaNoteManager;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info(LogUtil.METHOD_ENTER.concat("GET"));
        List<TeaNoteEntity> topTeaNoteList = teaNoteManager.getTopTeaNotes();
        req.setAttribute("teaNotes", topTeaNoteList);
        log.info(LogUtil.METHOD_SUCCESS.concat("GET"));
        req.getRequestDispatcher("/jsp/common/topNotes.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
