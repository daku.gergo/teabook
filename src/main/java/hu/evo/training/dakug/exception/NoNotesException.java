package hu.evo.training.dakug.exception;

public class NoNotesException extends TeaNotesExeption {

    public NoNotesException() {
        super(TeaNotesExeption.NO_NOTES);
    }
}
