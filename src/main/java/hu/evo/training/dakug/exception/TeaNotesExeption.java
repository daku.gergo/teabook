package hu.evo.training.dakug.exception;

public abstract class TeaNotesExeption extends Exception {

    public static final String WRONG_CREDENTIALS= "Hibás user/jelszó páros!";
    public static final String UNKNOWN   = "Váratlan hiba!";
    public static final String WRONG_SCORE   = "Nem megfelelő értékelés!";
    public static final String NO_NOTES = "Még nincsenek hozzáadva jegyezetek";

    public synchronized Throwable fillInStackTrace()  { return this; }

    public TeaNotesExeption(String message) {
        super(message);
    }

}
