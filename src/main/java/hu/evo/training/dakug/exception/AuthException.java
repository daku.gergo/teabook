package hu.evo.training.dakug.exception;

public class AuthException extends TeaNotesExeption {

    public AuthException(){
        super(TeaNotesExeption.WRONG_CREDENTIALS);
    }
}
