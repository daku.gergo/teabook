package hu.evo.training.dakug.exception;

public class TechnicalException extends TeaNotesExeption {

    public TechnicalException() {
        super(TeaNotesExeption.UNKNOWN);
    }
}
