package hu.evo.training.dakug.exception;

public class ScoreException extends TeaNotesExeption {

    public ScoreException() {
        super(TeaNotesExeption.WRONG_SCORE);
    }
}
