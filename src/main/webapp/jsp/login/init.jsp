<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>TeaNote bejelentkezés</title>
</head>
<body>

<h1>Bejelentkezés</h1>
<form action="login" method="post">
    user: <br /><input name="user"><br />
    password: <br /><input name="password" type="password"><br />
    <input type="submit" value="Bejelentkezés">

    <h3>
        <%
            if(request.getAttribute("result_message") != null) {
                out.println((String) request.getAttribute("result_message"));
            }
        %>
    </h3>
</form>
</body>
</html>