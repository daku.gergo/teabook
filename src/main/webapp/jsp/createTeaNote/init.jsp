<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Jegyzet létrehozása</title>
</head>
<body>
    <form action="${pageContext.request.contextPath}/createTeaNote" method = post>
        Tea neve:
        <select name="teaId">
            <c:forEach var="tea" items="${teas}">
                <option value="${tea.id}">${tea.name}</option>
            </c:forEach>
        </select>
        Értékelés(0-100):
        <input type="number" name="score">
        Főzési hőmérséklete:
        <input type="number" name="temperature">
        Ásványi anyag tartalom(opcionális):
        <input type="number" name="mineral">
        Megjegyzés(opcionális):
        <input name="note" size="250">
        <br/>
        <input type="submit">
    </form>
</body>
</html>
