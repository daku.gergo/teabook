<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Főmenü</h1>

<div>
    <p>
    <table>
        <tr>
            <td>
                <a href="${pageContext.request.contextPath}/login">
                    Bejelentkezés
                </a>
            </td>
        </tr>
        <tr>
            <td>
                <a href="${pageContext.request.contextPath}/topNotes">
                    Top 3 jegyzet
                </a>
            </td>
        </tr>
        <tr>
            <td>
                <a href="${pageContext.request.contextPath}/topUsers">
                    Top 3 felhasználó
                </a>
            </td>
        </tr>
        <tr>
            <td>
                <a href="${pageContext.request.contextPath}/allTeaNotes">
                    Böngészés
                </a>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
