<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Saját jegyzetek</title>
</head>
<body>
    <table border="1">
        <thead>
        <th>Név</th>
        <th>Étékelés(1-100)</th>
        <th>Létrehozás Dátuma</th>
        </thead>
        <tbody>
        <c:forEach items="${teanotes}" var="teanote">
            <tr>
                <td ><a href = "${pageContext.request.contextPath}/editTeaNote/?teanoteid=${teanote.id}"> ${teanote.teaName} </a></td>
                <td>${teanote.score}</td>
                <td>${teanote.createDate}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
<br/>
    <h3>
    <%
        if((String) request.getAttribute("result_message") != null){
            out.println((String) request.getAttribute("result_message"));
        }
    %>
    </h3>
    <br/>
    <jsp:include page="../loggedInFooter.jsp"/>
</body>
</html>
