<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Jegyzet szerkesztéses</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/editTeaNote" method = post>
    Tea neve:
    <select name="teaId">
        <c:forEach var="tea" items="${teas}">
            <option value="${tea.id}" >${tea.name}</option> <!-- a selecteedet meg kell csinálni  "$"{tea.id == teanote.teaTypeId ? selected="selected" : ''}  #TODO-->
        </c:forEach>
    </select>
    Értékelés(0-100):
    <input type="number" name="score" value="${teanote.score}" >
    Főzési hőmérséklete:
    <input type="number" name="temperature" value="${teanote.waterTemp}">
    Ásványi anyag tartalom(opcionális):
    <input type="number" name="mineral" value="${teanote.score}">
    Megjegyzés(opcionális):
    <input name="note" size="250" value="${teanote.note}">
    <br/>
    <input type="hidden" name="teanoteid" value="${teanote.id}">
    <input type="submit" value="Módosítások mentése">
</form>
<br/>

<jsp:include page="../loggedInFooter.jsp"/>
</body>
</html>
