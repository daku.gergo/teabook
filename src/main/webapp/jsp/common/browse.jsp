<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Böngészés</title>
</head>
<body>
<h3>Az összes jegyzet</h3>
<form action="${pageContext.request.contextPath}/allTeaNotes"   method="post">
    Teatípus
    <select name="teaId">
        <option value="0" >Kérjük válasszon </option>
        <c:forEach var="tea" items="${teas}">
            <option value="${tea.id}">${tea.name}</option>
        </c:forEach>
    </select>
    <br/>
    Rendezés
    <select name="sortType">
        <c:forEach var="sortingType" items="${sortingTypes}">
            <option value="${sortingType.text}">${sortingType.text}</option>
        </c:forEach>
    </select>
    <br/>
    <br/>
    <input type="submit" value="Szűrés">
</form>
<table border="1">
    <tr>
        <th>Tea neve</th>
        <th>Értékelés</th>
        <th>Jegyzet létrehozásának dátuma</th>
        <th>Felhasználó neve</th>
    </tr>
    <c:forEach var="teaNote" items="${teaNotes}">
        <tr>
            <td>${teaNote.teaName}</td>
            <td>${teaNote.score}</td>
            <td>${teaNote.createDate}</td>
            <td>${teaNote.userEntity.login}</td>
        </tr>
    </c:forEach>
</table>
<br/>
<a href="${pageContext.request.contextPath}">
    Főoldal
</a>
</body>
</html>
