<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Top 3 jegyzet</title>
</head>
<body>
    <h3>Az Első 3 Jegyzet</h3>
    <table border="1">
        <tr>
            <th>Tea neve</th>
            <th>Értékelés</th>
            <th>Főzési hőmérséklet</th>
            <th>Ásványi anyag tartalom</th>
            <th>Megjegyzés</th>
        </tr>
        <c:forEach var="teaNote" items="${teaNotes}">
            <tr>
                <td>${teaNote.teaName}</td>
                <td>${teaNote.score}</td>
                <td>${teaNote.waterTemp}</td>
                <td>${teaNote.waterMineral}</td>
                <td>${teaNote.note}</td>
            </tr>
        </c:forEach>
    </table>
<br/>
    <a href="${pageContext.request.contextPath}">
        Főoldal
    </a>
</body>
</html>
