<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Top 3 felhasználó jegyzetei</title>
</head>
<body>
<h3>Top 3 felhasználó jegyzetei</h3>
    <table border="1">
        <tr>
            <th>Felhasználó neve</th>
            <th>Tea neve</th>
            <th>Értékelés</th>
        </tr>
        <c:forEach var="teaNote" items="${teaNotes}">
            <tr>
                <td>${teaNote.userEntity.login}</td>
                <td>${teaNote.teaName}</td>
                <td>${teaNote.score}</td>
            </tr>
        </c:forEach>
    </table>
<br/>
<a href="${pageContext.request.contextPath}">
    Főoldal
</a>
</body>
</html>
